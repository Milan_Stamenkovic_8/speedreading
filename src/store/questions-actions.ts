import { Action } from "redux";
import { TestQuestions } from "../models/TestQuestions";

export const FETCH_QUESTIONS = "Fetch Questions";
export const ADD_QUESTIONS = "Add Questions";

export interface FetchQuestions extends Action<string>{

}

export interface AddQuestions extends Action<string>{
    questions: TestQuestions[];
}

export function fetchQuestions(): FetchQuestions{
    return{
        type: FETCH_QUESTIONS
    };
}

export function addQuestions(questions: TestQuestions[]): AddQuestions{
    return{
        type: ADD_QUESTIONS,
        questions: questions
    }
}

export type Actions = FetchQuestions | AddQuestions;