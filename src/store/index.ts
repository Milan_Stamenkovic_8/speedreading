import { combineReducers } from "redux";
import UsersReducer from "./users-reducer";
import { User } from "../models/User";
import StoryReducer from "./stories-reducer";
import { Story } from "../models/Story";
import QuestionsReducer from "./questions-reducer";
import { TestQuestions } from "../models/TestQuestions";

export interface RootState{
    users: User[],
    stories: Story[],
    questions: TestQuestions[]
}

export const rootReducer = combineReducers({
    users: UsersReducer,
    stories: StoryReducer,
    questions: QuestionsReducer
})

