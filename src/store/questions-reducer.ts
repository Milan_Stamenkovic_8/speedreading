import { TestQuestions } from "../models/TestQuestions";
import { Actions, ADD_QUESTIONS, AddQuestions } from "./questions-actions";


const initialState: TestQuestions[] = [
    {
        id: 0,
        question: "Pitanje 1",
        answers: ["a) Odgovor 1", "b) Odgovor 2", "c) Odgovor 3", "d) Odgovor 4"],
        correctAnswer: "a"
    }
];

export default function reducer(state: TestQuestions[] = initialState, action: Actions){
    
    switch (action.type){
        case ADD_QUESTIONS: {
            const { questions } = action as AddQuestions;
            return [...state, ...questions];
        }
        default:
            return state;
    }
}