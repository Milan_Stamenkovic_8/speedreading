import {all, call, put, takeEvery } from 'redux-saga/effects';
import { FETCH_USERS, addUsers } from './users-actions';
import { getAllUsers } from '../services/users.service';
import { getStories } from '../services/stories.service';
import { FETCH_STORIES, addStories } from './stories-actions';
import { getTests } from '../services/tests.service';
import { FETCH_QUESTIONS, addQuestions } from './questions-actions';



function* fetchUsers(){
    const response = yield call(getAllUsers);
    yield put(addUsers(response));
}

function* fetchStories(){
    const response = yield call(getStories);
    yield put(addStories(response));
}


function* fetchQuestions(){
    const response = yield call(getTests);
    yield put(addQuestions(response));
}

export function* rootSaga() {
    yield all([
        takeEvery(FETCH_USERS, fetchUsers),
        takeEvery(FETCH_STORIES, fetchStories),
        takeEvery(FETCH_QUESTIONS, fetchQuestions)
    ]);
}