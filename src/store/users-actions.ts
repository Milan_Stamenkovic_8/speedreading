import { Action } from "redux";
import { User } from "../models/User";


export const FETCH_USERS = "Fetch Users";
export const CHECK_USER = "Check User";
export const ADD_USERS = "Add Users";
export const LOG_OUT = "Log Out";




export interface CheckUser extends Action<string>{
    id: string;
}

export interface FetchUsers extends Action<string>{

}   

export interface AddUsers extends Action<string>{
    users: User[];
}

export interface LogOut extends Action<string>{
    users: User[];
}


export function checkUser(id: string): CheckUser{
    return{
        type: CHECK_USER, 
        id
    };
}

export function fetchUsers(): FetchUsers{
    return {
        type: FETCH_USERS
    };
}


export function addUsers(users: User[]): AddUsers{
    return{
        type: ADD_USERS,
        users: users
    };
}

export function logOut(users: User[]): LogOut{
    return{
        type: LOG_OUT,
        users: users
    }
}




export type Actions =  FetchUsers | AddUsers | LogOut;