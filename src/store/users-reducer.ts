import { User } from '../models/User';
import { Actions, ADD_USERS, AddUsers, CHECK_USER, CheckUser, LOG_OUT, LogOut } from './users-actions';


export const initialState: User[] = [
        {
        id: "korisnik",
        username: "Korisnik",
        password: "password",
        speed: 0}    
    ];



export default function reducer(state: User[] = initialState, action: Actions){
    
    switch (action.type) {
        case ADD_USERS: {
            const { users } = action as AddUsers;
            return [...state, ...users];
        }
        case CHECK_USER: {
            const  { id } = action as CheckUser;
            return state.filter((user: User) => user.id === id);
        }
        case LOG_OUT: {
            const { users } = action as LogOut;
            return [...initialState, ...users]
        }
        default:
            return state;
    }


}