import { Story } from "../models/Story";
import { SHOW_STORY, ShowStory, Actions, ADD_STORIES, AddStories } from "./stories-actions";    



const initialState: Story[] = [
    {
        id: 0,
        title: "Naslov",
        text: "Priča",
        words: 2
    }
];

export default function reducer(state: Story[] = initialState, action: Actions){
    
    switch (action.type){
        case ADD_STORIES: {
            const { stories } = action as AddStories;
            return [...state, ...stories];
        }
        case SHOW_STORY: {
            const { storyId } = action as ShowStory;
            return state.filter((story: Story) => story.id === storyId);
        }
        default:
            return state;
    }
}