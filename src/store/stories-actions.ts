import { Action } from "redux";
import { Story } from "../models/Story";

export const FETCH_STORIES = "Fetch Stories";
export const ADD_STORIES = "Add Stories";
export const SHOW_STORY = "Show Story";



export interface FetchStories extends Action<string>{
    
}

export interface AddStories extends Action<string>{
    stories: Story[];
}


export interface ShowStory extends Action<string>{
    storyId: number;
}


export function fetchStories(): FetchStories{
    return{
        type: FETCH_STORIES
    };
}

export function addStories(stories: Story[]): AddStories{
    return{
        type: ADD_STORIES,
        stories: stories
    };
}


export function showStory(storyId: number): ShowStory{
    return{
        type: SHOW_STORY,
        storyId
    };
}



export type Actions = FetchStories | ShowStory | AddStories;