export interface TestQuestions{
    id: number;
    question: string;
    answers: Array<string>;
    correctAnswer: string;
}