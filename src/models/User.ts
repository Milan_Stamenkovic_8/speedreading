export interface User{
    id: string;
    username: string;
    password: string;
    speed: number;
}