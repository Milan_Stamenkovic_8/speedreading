import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import User from './components/UserInfo';
import About from './components/About';
import Home from './components/Home';
import Login from './components/Login';
import Signup from './components/Signup';
import createSagaMiddleware from '@redux-saga/core';
import { createStore, applyMiddleware } from 'redux';
import { rootReducer } from './store';
import { rootSaga } from './store/sagas';
import { Provider } from 'react-redux';
import ReadingPractice from './components/reading_practice/ReadingPractice';
import ReadingTest from './components/reading_practice/ReadingTest';
import UnderstandingTest from './components/understanding_test/UnderstandingTest';


export const sagaMiddleware = createSagaMiddleware();
export const store = createStore(
  rootReducer,
  applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(rootSaga);



class App extends Component{
  state = {
    loggedIn: false
  }
  render(){
  return (
    <Router>
    <Provider store = {store}>
    <div className = "App">
      <Route path = "/" exact component = { Home }/>
      <Route path = "/about" exact  component = { About }/>
      <Route path = "/user" exact  strict render = {() =>(
          store.getState().users[0].username !== "Korisnik" ? (<User/>) : (<Redirect to = "/"/>)
      )}/>
      <Route path = "/signup" exact  component = { Signup }/>
      <Route path = "/login" exact  component = { Login }/>
      <Route path = "/practice" exact  component = { ReadingPractice }/>
      <Route path = "/test" exact  strict  render = {() =>(
          store.getState().users[0].username !== "Korisnik" ? (<ReadingTest/>) : (<Redirect to = "/"/>)
      )}/>
      <Route path = "/understanding" exact  strict render = {() =>(
          store.getState().users[0].username !== "Korisnik" ? (<UnderstandingTest/>) : (<Redirect to = "/"/>)
      )}/>
    </div>
    </Provider>
    </Router>
  );
  }
}

export default App;
