import React, { Component } from "react";
import NavBar from "../Navbar";
import "../../scss/practice.css";
import { Provider, connect } from "react-redux";
import { store } from "../../App";
import { fetchQuestions } from "../../store/questions-actions";
import { TestQuestions } from "../../models/TestQuestions";
import { RootState } from "../../store";
import QuestionsContainer from "./QuestionsContainer";


interface State{
    testId: number;
}


interface Props{
    questions: TestQuestions[];
}

class UnderstandingTest extends Component<Props, State>{
    constructor(props:Props){
        super(props)
        this.state ={ testId: 0 }
    }
    render(){
        return(
            <>
            <NavBar />
            <div >
            <Provider store = {store}>
            <div>
            <label className = "labelChose">Izaberite test:</label>
            <button className = "btn btn-danger ucitajTestove" onClick = {() => {
               if(this.props.questions.length <= 1){
                    store.dispatch(fetchQuestions());
                    alert("Učitali ste potrebne testove.");
                }
               else{
                    alert("Već ste učitali potrebne testove. Izaberite željeni test i odgovorite na pitanja.")
               }
            }}>Učitaj Testove</button>
            </div>
            <select className = "selectStyle" onChange = {(event) => {
                if(this.props.questions.length <= 1){
                    alert("Morate prvo učitati testove.");
                    event.target.selectedIndex = 0;
                }
                else{
                    this.setState({testId: event.target.selectedIndex});
                }
            }}>
                <option>Izaberite test na osnovu teksta koji ste prethodno pročitali</option>
                <option>Test 1 - Novi svetski trendovi</option>
                <option>Test 2 - Umetnost - od primitivne do hrišćanske</option>
                <option>Test 3 - Životinjska inteligencija</option>
                <option>Test 4 - Da li smo sami u univerzumu? Inteligencija vanzemaljaca</option>
                <option>Test 5 - Mozak bebe</option>
                <option>Test 6 - Zemlja koja se budi: naš sledeći evolutivni skok - globalni mozak</option>
                <option>Test 7 - Vaš mozak - začarani razboj</option>
            </select>
            <div>
            <QuestionsContainer  testId = {this.state.testId}/>
            </div>
            </Provider>
            </div>
            </>
        )
    }
}


function mapStateToProps(state: RootState) {
    return {
      questions: state.questions
    };
}

export default connect(mapStateToProps)(UnderstandingTest);