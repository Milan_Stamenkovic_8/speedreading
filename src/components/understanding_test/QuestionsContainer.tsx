import React, { Component } from "react";
import "../../scss/practice.css";       
import { TestQuestions } from "../../models/TestQuestions";
import { RootState } from "../../store";
import { connect } from "react-redux";




interface State{
    firstTestQuestionNumber: number;
    secondTestQuestionNumber: number;
    thirdTestQuestionNumber: number;
    fourthTestQuestionNumber: number;
    fifthTestQuestionNumber: number;
    sixthTestQuestionNumber: number;
    seventhTestQuestionNumber: number;
    correctAnswers: number;
}

interface Props{
    questions: TestQuestions[];
    testId: number;
}

const TEST_NUMBER = {
    test1: 1,
    test2: 2,
    test3: 3,
    test4: 4,
    test5: 5,
    test6: 6,
    test7: 7
};

class QuestionsContainer extends Component<Props, State>{
    constructor(props:Props){
        super(props)
        this.state ={ firstTestQuestionNumber: 1, secondTestQuestionNumber: 16, 
                      thirdTestQuestionNumber: 30, fourthTestQuestionNumber: 45, fifthTestQuestionNumber: 60,
                      sixthTestQuestionNumber: 75, seventhTestQuestionNumber: 90, correctAnswers: 0 }
    }

    setQuestionNumber(){
        switch(this.props.testId){
            case TEST_NUMBER.test1:{
                this.setState({firstTestQuestionNumber: this.state.firstTestQuestionNumber + 1});
                break;
            }
            case TEST_NUMBER.test2:{
                this.setState({secondTestQuestionNumber: this.state.secondTestQuestionNumber + 1});
                break;
            }
            case TEST_NUMBER.test3:{
                this.setState({thirdTestQuestionNumber: this.state.thirdTestQuestionNumber + 1});
                break;
            }
            case TEST_NUMBER.test4:{
                this.setState({fourthTestQuestionNumber: this.state.fourthTestQuestionNumber + 1});
                break;
            }
            case TEST_NUMBER.test5:{
                this.setState({fifthTestQuestionNumber: this.state.fifthTestQuestionNumber + 1});
                break;
            }
            case TEST_NUMBER.test6:{
                this.setState({sixthTestQuestionNumber: this.state.sixthTestQuestionNumber + 1});
                break;
            }
            case TEST_NUMBER.test7:{
                this.setState({seventhTestQuestionNumber: this.state.seventhTestQuestionNumber + 1});
                break;
            }
        }
    }

    testRender(questionNo: number){
        if(this.props.questions[questionNo].answers.length < 3){
        return(
            <div className = "questionsContainer">
                    <label className = "questionLabel">{this.props.questions[questionNo].question}</label>
                    <div className = "answersContainer">
                        <button className = "btn btn-dark answerButton" onClick = {() =>{
                            this.setQuestionNumber()
                            if(this.props.questions[questionNo].answers[0].substring(0, 1) === 
                                this.props.questions[questionNo].correctAnswer.substring(0, 1) ){
                                this.setState({correctAnswers: this.state.correctAnswers + 1})
                            }
                        }}>{this.props.questions[questionNo].answers[0]}</button>
                        <button className = "btn btn-dark answerButton" onClick = {() =>{
                            this.setQuestionNumber()
                            if(this.props.questions[questionNo].answers[1].substring(0, 1) === 
                                this.props.questions[questionNo].correctAnswer.substring(0, 1) ){
                                this.setState({correctAnswers: this.state.correctAnswers + 1})
                            }
                        }}>{this.props.questions[questionNo].answers[1]}</button>
                    </div>
            </div>
        )}
        else{
            return(
                <div className = "questionsContainer">
                        <label className = "questionLabel">{this.props.questions[questionNo].question}</label>
                        <div className = "answersContainer">
                            <button className = "btn btn-dark answerButton" onClick = {() =>{
                                this.setQuestionNumber()
                                if(this.props.questions[questionNo].answers[0].substring(0, 1) === 
                                this.props.questions[questionNo].correctAnswer ){
                                this.setState({correctAnswers: this.state.correctAnswers + 1})
                            }
                            }}>{this.props.questions[questionNo].answers[0]}</button>
                            <button className = "btn btn-dark answerButton" onClick = {() =>{
                                this.setQuestionNumber()
                                if(this.props.questions[questionNo].answers[1].substring(0, 1) === 
                                this.props.questions[questionNo].correctAnswer ){
                                this.setState({correctAnswers: this.state.correctAnswers + 1})
                            }
                            }}>{this.props.questions[questionNo].answers[1]}</button><br></br>
                            <button className = "btn btn-dark answerButton" onClick = {() =>{
                                this.setQuestionNumber()
                                if(this.props.questions[questionNo].answers[2].substring(0, 1) === 
                                this.props.questions[questionNo].correctAnswer ){
                                this.setState({correctAnswers: this.state.correctAnswers + 1})
                            }
                            }}>{this.props.questions[questionNo].answers[2]}</button>
                            <button className = "btn btn-dark answerButton" onClick = {() =>{
                                this.setQuestionNumber()
                                if(this.props.questions[questionNo].answers[3].substring(0, 1) === 
                                this.props.questions[questionNo].correctAnswer ){
                                this.setState({correctAnswers: this.state.correctAnswers + 1})
                            }
                            }}>{this.props.questions[questionNo].answers[3]}</button>
                        </div>
                </div>
        )}
    }
    render(){ 
        switch(this.props.testId){
            case TEST_NUMBER.test1: {
                if(this.state.firstTestQuestionNumber < 16){
                return(
                    this.testRender(this.state.firstTestQuestionNumber)
                )}
                else{
                return(
                    <div className = "testResultContainer">
                        <label className = "testResultLabel">Broj pitanja: {this.state.firstTestQuestionNumber - 1}</label>
                        <label className = "testResultLabel">Broj tačnih odgovora: {this.state.correctAnswers}</label>
                        <label className = "testResultLabel">
                        Procenat razumevanja: {((this.state.correctAnswers/(this.state.firstTestQuestionNumber - 1))*100).toFixed(2)}%
                        </label>
                        <button className = "btn btn-danger closeButton" onClick = {() => {
                            this.setState({firstTestQuestionNumber: 1, correctAnswers: 0})
                        }}>Zatvori</button>
                    </div>
                )
                }
            }
            case TEST_NUMBER.test2: {
                if(this.state.secondTestQuestionNumber < 30){
                return(
                    this.testRender(this.state.secondTestQuestionNumber)
                )}
                else{
                    return(
                        <div className = "testResultContainer">
                            <label className = "testResultLabel">Broj pitanja: {this.state.secondTestQuestionNumber - 16}</label>
                            <label className = "testResultLabel">Broj tačnih odgovora: {this.state.correctAnswers}</label>
                            <label className = "testResultLabel">
                            Procenat razumevanja: {((this.state.correctAnswers/(this.state.secondTestQuestionNumber - 16))*100).toFixed(2)}%
                            </label>
                            <button className = "btn btn-danger closeButton" onClick = {() => {
                                this.setState({secondTestQuestionNumber: 16, correctAnswers: 0})
                            }}>Zatvori</button>
                        </div>
                    )
                }
            }
            case TEST_NUMBER.test3: {
                if(this.state.thirdTestQuestionNumber < 45){
                return(
                    this.testRender(this.state.thirdTestQuestionNumber)
                )}
                else{
                    return(
                        <div className = "testResultContainer">
                            <label className = "testResultLabel">Broj pitanja: {this.state.thirdTestQuestionNumber - 30}</label>
                            <label className = "testResultLabel">Broj tačnih odgovora: {this.state.correctAnswers}</label>
                            <label className = "testResultLabel">
                            Procenat razumevanja: {((this.state.correctAnswers/(this.state.thirdTestQuestionNumber - 30))*100).toFixed(2)}%
                            </label>
                            <button className = "btn btn-danger closeButton" onClick = {() => {
                                this.setState({thirdTestQuestionNumber: 30, correctAnswers: 0})
                            }}>Zatvori</button>
                        </div>
                    )
                }
            }
            case TEST_NUMBER.test4: {
                if(this.state.fourthTestQuestionNumber < 60){
                return(
                    this.testRender(this.state.fourthTestQuestionNumber)
                )}
                else{
                    return(
                        <div className = "testResultContainer">
                        <label className = "testResultLabel">Broj pitanja: {this.state.fourthTestQuestionNumber - 45}</label>
                        <label className = "testResultLabel">Broj tačnih odgovora: {this.state.correctAnswers}</label>
                        <label className = "testResultLabel">
                        Procenat razumevanja: {((this.state.correctAnswers/(this.state.fourthTestQuestionNumber - 45))*100).toFixed(2)}%
                        </label>
                        <button className = "btn btn-danger closeButton" onClick = {() => {
                            this.setState({fourthTestQuestionNumber: 45, correctAnswers: 0})
                        }}>Zatvori</button>
                    </div>
                    )
                }
            }
            case TEST_NUMBER.test5: {
                if(this.state.fifthTestQuestionNumber < 75){
                return(
                    this.testRender(this.state.fifthTestQuestionNumber)
                )}
                else{
                    return(
                        <div className = "testResultContainer">
                        <label className = "testResultLabel">Broj pitanja: {this.state.fifthTestQuestionNumber - 60}</label>
                        <label className = "testResultLabel">Broj tačnih odgovora: {this.state.correctAnswers}</label>
                        <label className = "testResultLabel">
                        Procenat razumevanja: {((this.state.correctAnswers/(this.state.fifthTestQuestionNumber - 60))*100).toFixed(2)}%
                        </label>
                        <button className = "btn btn-danger closeButton" onClick = {() => {
                            this.setState({fifthTestQuestionNumber: 60, correctAnswers: 0})
                        }}>Zatvori</button>
                    </div>
                    )
                }
            }
            case TEST_NUMBER.test6: {
            if(this.state.sixthTestQuestionNumber < 90){
                return(
                    this.testRender(this.state.sixthTestQuestionNumber)
                )}
                else{
                    return(
                        <div className = "testResultContainer">
                        <label className = "testResultLabel">Broj pitanja: {this.state.sixthTestQuestionNumber - 75}</label>
                        <label className = "testResultLabel">Broj tačnih odgovora: {this.state.correctAnswers}</label>
                        <label className = "testResultLabel">
                        Procenat razumevanja: {((this.state.correctAnswers/(this.state.sixthTestQuestionNumber - 75))*100).toFixed(2)}%
                        </label>
                        <button className = "btn btn-danger closeButton" onClick = {() => {
                            this.setState({sixthTestQuestionNumber: 75, correctAnswers: 0})
                        }}>Zatvori</button>
                    </div>
                    )
                }
            }
            case TEST_NUMBER.test7: {
                if(this.state.seventhTestQuestionNumber < 105){
                return(
                    this.testRender(this.state.seventhTestQuestionNumber)
                )}
                else{
                    return(
                        <div className = "testResultContainer">
                        <label className = "testResultLabel">Broj pitanja: {this.state.seventhTestQuestionNumber - 90}</label>
                        <label className = "testResultLabel">Broj tačnih odgovora: {this.state.correctAnswers}</label>
                        <label className = "testResultLabel">
                        Procenat razumevanja: {((this.state.correctAnswers/(this.state.seventhTestQuestionNumber - 90))*100).toFixed(2)}%
                        </label>
                        <button className = "btn btn-danger closeButton" onClick = {() => {
                            this.setState({seventhTestQuestionNumber: 90, correctAnswers: 0})
                        }}>Zatvori</button>
                    </div>
                    )
                }
            }
            default:
                return(
                <div className = "questionsContainer">
                    <label className = "questionLabel">{this.props.questions[0].question}</label>
                        <div className = "answersContainerDefault">
                        <button className = "btn btn-dark answerButton">{this.props.questions[0].answers[0]}</button>
                        <button className = "btn btn-dark answerButton">{this.props.questions[0].answers[1]}</button><br></br>
                        <button className = "btn btn-dark answerButton">{this.props.questions[0].answers[2]}</button>
                        <button className = "btn btn-dark answerButton">{this.props.questions[0].answers[3]}</button>
                        </div>
                    <label className = "questionLabel">Kliknite na odgovor za koji mislite da je tačan.</label>
                </div>
                )
        }
    }
}


function mapStateToProps(state: RootState) {
    return {
      questions: state.questions
    };
}

export default connect(mapStateToProps)(QuestionsContainer);