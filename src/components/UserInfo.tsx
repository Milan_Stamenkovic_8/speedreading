import React, { Component } from "react";
import NavBar from "./Navbar";
import Particles from "react-particles-js";
import { particlesOpt } from './particles';
import UserDetails from "./UserDetails";
import '../scss/user.css';


class UserInfo extends Component{
    render(){
        return(
            
            <>
            <NavBar />
            <div>
            <Particles className="particles" params= {particlesOpt} />
            <UserDetails></UserDetails>
            </div>
            </>
        );
    }
}


  

export default UserInfo;