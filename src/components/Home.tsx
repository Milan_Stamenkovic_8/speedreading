import React, { Component } from "react";
import NavBar from "./Navbar";
import { Link } from "react-router-dom";
import '../scss/home.css';
import Particles from 'react-particles-js';
import { particlesOpt } from './particles';
import StartingCards from "./starting_cards/StartingCards";
import '../scss/starting_card.css'
import { Provider, connect } from "react-redux";
import { store } from "../App";
import { fetchUsers } from "../store/users-actions";
import { User } from "../models/User";
import { RootState } from "../store";


interface Props{
    users: User[];
}

interface State{
}


class Home extends Component<Props, State>{
    render(){
        return(
            <>
            <NavBar />
            <div >
                
            <Particles className="particles" params= {particlesOpt} />
            <Provider store = {store}>
            <Link to="/signup" className="navbar-brand btn btn-primary" onClick = { () => {
            if(this.props.users.length <= 1){
                store.dispatch(fetchUsers())
                }
            }}>Kreiraj nalog</Link>
            <Link to="/login" className="navbar-brand btn btn-primary" onClick = { () => {
            if(this.props.users.length <= 1){
                store.dispatch(fetchUsers())
                } 
            }}>Prijavi se</Link>
            </Provider>
            <div className= "containerForCards">
            <StartingCards></StartingCards>
            </div>
            
            </div>
            </>
        )
    }
}


function mapStateToProps(state: RootState){
    return{
        users: state.users
    };
}

export default connect(mapStateToProps)(Home);


