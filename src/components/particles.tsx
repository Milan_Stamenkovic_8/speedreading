export const particlesOpt = {
    particles: {
        number: {
          value: 150,
          density: {
            enable: true,
            value_area: 547.0220103698913
          }
        }, 
        opacity: {
            value: 1,
            random: true,
            anim: {
                enable: false,
                speed: 1
            }
        },
        size: {
            value: 2,
            random: false,
            anim: {
                enable: false,
                speed:30
            }
        },
        line_linked: {
            enable: true,
            distance: 120,
            color: "#fff",
            width: 2
        },
        move: {
            enable: true,
            speed: 3
        }
    },
    interactivity: {
        events: {
            onhover: {
                enable: true
            },
            onclick: {
                enable: true
            },
        },
        modes: {
            repulse: {
                distance: 50,
                duration: 0.3
            }
        }
    }
}