import React, { Component } from "react";
import { User } from "../models/User";
import { RootState } from "../store";
import { connect, Provider } from "react-redux";
import '../scss/user.css';
import { deleteUser } from "../services/users.service";
import { store } from "../App";
import { logOut } from "../store/users-actions";
import { initialState } from "../store/users-reducer";
import { RouteComponentProps, withRouter } from "react-router-dom";

interface State {
}


interface Props extends RouteComponentProps<any> {
    users: User[]
}

class UserDetails extends Component<Props, State> {
    render(){
        if(!this.props.users){
            return "";
        }
        return(
            <>
            <div className= "userinfo">
            <label>Username: {this.props.users[0].username}</label><br></br>
            <label>Brzina čitanja: {this.props.users[0].speed.toFixed(2)} reči/minutu</label>
            </div>
            <div className = "deleteButtonContainer">
            <Provider store = {store}>
            <button className = "btn btn-danger" onClick = {() => {
                if(window.confirm("Da li ste sigurni da želite da obrišete nalog?")){
                    deleteUser(this.props.users[0].id);
                    store.dispatch(logOut(initialState));
                    this.props.history.push('/');
                }
            }}>Obriši nalog</button>
            </Provider>
            </div>
            </>
        )
    }

}

function mapStateToProps(state: RootState) {
    return {
      users: state.users
    };
}

export default connect(mapStateToProps)(withRouter(UserDetails));