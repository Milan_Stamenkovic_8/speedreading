import React, { Component } from "react";
import "../../scss/practice.css";
import { Provider, connect } from "react-redux";
import { store } from "../../App";
import { fetchStories } from "../../store/stories-actions";
import { RootState } from "../../store";
import { Story } from "../../models/Story";


interface Props{
    stories: Story[];
}

interface State{
    storyId: number;
    readingSpeed: number;
    readingSpeedSeconds: number;
    textWords: Array<string>;
    wordtoHighlight: string;
    textWordsIndex: number;
    textIndex: number;
    startPausebuttonText: string;
    intervalID: ReturnType<typeof setTimeout>;
    intervalValue: number;
    start: boolean;
    firstPartText: string;
    secondPartText: string;
    thirdPartText: string;
}

class PracticeLoad extends Component<Props, State>{
    constructor(props:Props){
        super(props)
        this.state ={ storyId: 0, readingSpeed: 120, readingSpeedSeconds: 2, textWords: [""], wordtoHighlight: "",
                      textWordsIndex: 0, textIndex: 0,
                      startPausebuttonText: "Kreni",
                      intervalID: setInterval(() => {}, 500), intervalValue: 500,
                      start: false, firstPartText: "", secondPartText: "", thirdPartText: ""
                    }
    }
    componentWillUnmount() {
        clearInterval(this.state.intervalID);
    }

    startIntervalAndReading(){
        this.setState({intervalID:setInterval(() => {
            this.setState({wordtoHighlight: this.state.textWords[this.state.textWordsIndex]});
            this.setState({textWordsIndex: this.state.textWordsIndex + 1});
            this.setState({
                firstPartText: this.props.stories[this.state.storyId].text.substring(
                    this.state.textIndex, 0
                ),
                secondPartText: this.state.wordtoHighlight,
                thirdPartText: this.props.stories[this.state.storyId].text.substring(
                    this.props.stories[this.state.storyId].text.length,
                    this.state.textIndex + this.state.wordtoHighlight.length 
                )});
                this.setState({textIndex: this.state.wordtoHighlight.length + this.state.textIndex + 1});
        }, this.state.intervalValue), });
    }

    setNewStoryValues(){
        setTimeout(()=>{
            this.setState({textWords: this.props.stories[this.state.storyId].text.split(" "),
            wordtoHighlight: this.state.textWords[this.state.textWordsIndex], start: true });
            this.setState({wordtoHighlight: this.state.textWords[this.state.textWordsIndex]});
            this.setState({
                firstPartText: this.props.stories[this.state.storyId].text.substring(
                    this.state.textIndex, 0
                ),
                secondPartText: this.state.wordtoHighlight,
                thirdPartText: this.props.stories[this.state.storyId].text.substring(
                    this.props.stories[this.state.storyId].text.length,
                    this.state.textIndex + this.state.wordtoHighlight.length 
                )})
            }, 10);
        clearInterval(this.state.intervalID);
    }

    restartValues(){
        this.setState({textWordsIndex: 0, textIndex: 0, startPausebuttonText: "Kreni"});
        setTimeout(() => {
            this.setState({ wordtoHighlight: this.state.textWords[this.state.textWordsIndex]
                })
        }, 30);
        setTimeout(() => {
            this.setState({
                firstPartText: this.props.stories[this.state.storyId].text.substring(
                    this.state.textIndex, 0
                ),
                secondPartText: this.state.wordtoHighlight,
                thirdPartText: this.props.stories[this.state.storyId].text.substring(
                    this.props.stories[this.state.storyId].text.length,
                    this.state.textIndex + this.state.wordtoHighlight.length 
                )});
        }, 60);
        clearInterval(this.state.intervalID);
    }

    render(){
        return(
            <>
            <div className = "container practiceContainer">
                <div className = "row">
                    <div className = "col mb-1">
                    <button className="btn-l btn btn-danger" onClick ={() => {
                        if(this.state.startPausebuttonText === "Kreni" && this.props.stories.length > 1 && this.state.start){
                            this.startIntervalAndReading();
                            this.setState({startPausebuttonText: "Pauza"});
                        }
                        else{
                            clearInterval(this.state.intervalID)
                            this.setState({startPausebuttonText: "Kreni"});
                        }
                        }}>
                            {this.state.startPausebuttonText}
                    </button>
                        <button className="btn btn-l btn-danger" onClick = {() =>{
                           if(this.state.start)
                           this.restartValues();
                        }}>Vrati na početak</button>
                        <button className="btn btn-r btn-danger" onClick = {() => {
                            if(this.state.readingSpeed < 1000 && this.state.start && this.state.startPausebuttonText === "Pauza"){
                            this.setState({readingSpeed: this.state.readingSpeed + 20, 
                                           readingSpeedSeconds: this.state.readingSpeed / 60,
                                           intervalValue: 1000 / this.state.readingSpeedSeconds})
                            clearInterval(this.state.intervalID);
                            setTimeout(() => {
                                this.startIntervalAndReading();
                            }, 100)
                        }}}>Ubrzaj</button>
                    </div>
                </div>
                <div className = "textInfoContainer">
                    <label className = "oneLine">Brzina reči/minutu: {this.state.readingSpeed}</label>
                    <label className = "oneLine">Broj reči u tekstu: {this.props.stories[this.state.storyId].words}</label>
                    <label className = "oneLine">Vreme potrebno da se pročita: {(this.props.stories[this.state.storyId].words /
                                                                                 this.state.readingSpeed).toFixed(2) + " minuta"}</label>
                </div>
                <div className = "row">
                    <div className = "col mb-1">
                    <Provider store = {store}>
                        <button className="btn btn-l btn-danger" onClick = {() => {
                           if(this.props.stories.length <= 1){
                            store.dispatch(fetchStories());
                            alert("Učitali ste sve tekstove.");
                           }
                           else{
                            alert("Već ste učitali potrebne tekstove. Kliknite na dugme \"Nova Priča\".");
                           }
                        }}>Učitaj Priče</button>
                        <button className="btn btn-l btn-danger" onClick = {()=> {
                        if(this.props.stories.length <= 1){
                            alert("Morate prvo učitati potrebne tekstove. Kliknite na dugme \"Učitaj Priče\".");
                        }
                        else{
                            if(this.state.storyId < this.props.stories.length-1){
                                this.setState({storyId: this.state.storyId + 1, textWordsIndex: 0,
                                               textIndex: 0, startPausebuttonText: "Kreni" });
                                this.setNewStoryValues();
                            }
                            else{
                                this.setState({storyId: 1, textWordsIndex: 0, textIndex: 0, startPausebuttonText: "Kreni" });
                                this.setNewStoryValues();
                            }
                        }   
                        }}>Nova Priča</button>
                    </Provider>
                        <button className="btn btn-r btn-danger" onClick = {() => {
                            if(this.state.readingSpeed > 120 && this.state.start && this.state.startPausebuttonText === "Pauza"){
                            this.setState({readingSpeed: this.state.readingSpeed - 20, 
                                readingSpeedSeconds: this.state.readingSpeed / 60,
                                intervalValue: 1000 / this.state.readingSpeedSeconds});
                                clearInterval(this.state.intervalID);
                                setTimeout(() => {
                                    this.startIntervalAndReading();
                                }, 100);
                            }
                        }}>Uspori</button>
                    </div>
                </div>
            </div>
            <div className = "tekstzavezbu container">
              <div>
              <h4 className = "text-center">{this.props.stories[this.state.storyId].title}</h4>
              <div>
              {this.state.firstPartText}
              <span className = "highlightedText">{this.state.secondPartText}</span>
              {this.state.thirdPartText}</div>
              </div>
            </div>
            </>
        )
    }
}


function mapStateToProps(state: RootState){
    return{
        stories: state.stories
    };
}

export default connect(mapStateToProps)(PracticeLoad);