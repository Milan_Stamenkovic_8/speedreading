import React, { Component } from "react";
import NavBar from "../Navbar";
import "../../scss/practice.css";
import { Story } from "../../models/Story";
import { Provider, connect } from "react-redux";
import { store } from "../../App";
import { fetchStories } from "../../store/stories-actions";
import { RootState } from "../../store";
import { User } from "../../models/User";
import { updateUser } from "../../services/users.service";


interface Props{
    stories: Story[];
    users: User[];
}

interface State{
    storyId: number;
    minute: number;
    second: number;
    start: boolean;
    buttonStartClassName: string;
    buttonFinishClassName: string;
    timerClassName: string;
    textContainerClassName: string;
    resultClassName: string;
    disableButton: boolean;
    intervalID: ReturnType<typeof setTimeout>;
    readingSpeed: number;
}


class ReadingTest extends Component<Props, State>{
    constructor(props:Props){
        super(props)
        this.state ={ storyId: 0, minute: 0, second: 0, start: false, 
                      buttonStartClassName: "btn btn-danger rightside", buttonFinishClassName: "btn btn-danger finishButton",
                      timerClassName: "timerContainer", textContainerClassName: "tekstzavezbu container", 
                      resultClassName: "resultDiv invisibility",
                      disableButton: true, intervalID: setInterval(() => {}, 0), readingSpeed: 0 }
    }
    componentWillUnmount() {
        clearInterval(this.state.intervalID);
    }
    render(){
        return(
            <>
            <NavBar />
            <div className = "container practiceContainer">
            <div className = "row">
                    <div className = "col mb-1">
                    <Provider store = {store}>
                        <button className="btn btn-l btn-danger" onClick = {() => {
                           if(this.props.stories.length <= 1){
                            store.dispatch(fetchStories());
                            alert("Učitali ste sve tekstove.");
                           }
                           else{
                            alert("Već ste učitali potrebne tekstove. Kliknite na dugme \"Nova Priča\".");
                           }
                        }}>Učitaj Priče</button>
                        <button className="btn btn-l btn-danger" onClick = {()=> {
                        if(this.props.stories.length <= 1){
                            alert("Morate prvo učitati potrebne tekstove. Kliknite na dugme \"Učitaj Priče\".");
                        }
                        else{
                            if(this.state.storyId < this.props.stories.length-1){
                                this.setState({storyId: this.state.storyId + 1, minute: 0, second: 0, 
                                               buttonStartClassName: "btn btn-danger rightside",
                                               buttonFinishClassName: "btn btn-danger finishButton",
                                               textContainerClassName: "tekstzavezbu container",
                                               timerClassName: "timerContainer",
                                               resultClassName: "resultDiv invisibility",
                                               disableButton: false, start: true});
                                clearInterval(this.state.intervalID);
                            }
                            else{
                                this.setState({storyId: 1, minute: 0, second: 0, buttonStartClassName: "btn btn-danger rightside", 
                                               disableButton: false, start: true, buttonFinishClassName: "btn btn-danger finishButton",
                                               textContainerClassName: "tekstzavezbu container",
                                               resultClassName: "resultDiv invisibility",
                                               timerClassName: "timerContainer" });
                                clearInterval(this.state.intervalID);
                            }
                        }   
                        }}>Nova Priča</button>
                        <div className = {this.state.timerClassName}> 
                        <p className = "timer">{("0" + this.state.minute).slice(-2)}:{("0" + this.state.second).slice(-2)}</p>
                        <button className= {this.state.buttonStartClassName} disabled = {this.state.disableButton} onClick = {() => {
                        if(this.state.start && this.props.stories.length > 1){
                            this.setState({intervalID:setInterval(() => {
                                    if(this.state.second >= 59){
                                        this.setState({second: 0, minute: this.state.minute + 1});
                                    }
                                    else{
                                        this.setState({second: this.state.second + 1})
                                    }
                            }, 1000), });
                            this.setState({buttonStartClassName: "btn btn-danger invisibility", start: false})
                        }
                        else{
                            if(this.state.storyId < 1){
                                alert("Izaberite pricu")
                            }
                        }
                        }}>
                        Kreni
                        </button>
                        
                        </div>
                    </Provider>
                    </div>
            </div>
            <div className = {this.state.textContainerClassName}>
              <div>
                <h4 className = "text-center">{this.props.stories[this.state.storyId].title}</h4>
                <p>{this.props.stories[this.state.storyId].text}</p>
              </div>
            </div>
            <button className = {this.state.buttonFinishClassName} disabled = {this.state.disableButton} onClick = {() => {
                if(!this.state.start){
                    clearInterval(this.state.intervalID);
                    this.setState({buttonFinishClassName: "btn btn-danger invisibility", 
                                   textContainerClassName: "tekstzavezbu container invisibility",
                                   timerClassName: "timerContainer invisibility",
                                   resultClassName: "resultShow", readingSpeed:
                                   ((this.props.stories[this.state.storyId].words / (this.state.second + 60 * this.state.minute)) * 60)
                                });
                    setTimeout(() => {
                    const userToUpdate: User ={
                        id: this.props.users[0].id,
                        username: this.props.users[0].username,
                        password: this.props.users[0].password,
                        speed: this.state.readingSpeed
                    }
                    if(this.props.users[0].speed < this.state.readingSpeed){
                        updateUser(this.props.users[0].id, userToUpdate);
                    }}, 200)
                }
            }}>Gotovo</button>


            <div className = {this.state.resultClassName}>
                <p>Broj reči u tekstu: {this.props.stories[this.state.storyId].words}</p>
                <p>Vreme za koje ste pročitali tekst: {("0" + this.state.minute).slice(-2)}:{("0" + this.state.second).slice(-2)}</p>
                <p>Brzina čitanja: {((this.props.stories[this.state.storyId].words / (this.state.second + 60 * this.state.minute)) * 60)
                .toFixed(2)} reči/minutu</p>
            </div>
            </div>
            </>
        )
    }
}


function mapStateToProps(state: RootState){
    return{
        stories: state.stories,
        users: state.users
    };
}

export default connect(mapStateToProps)(ReadingTest);