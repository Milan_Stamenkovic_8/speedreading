import React, { Component } from "react";
import NavBar from "./Navbar";
import { User } from "../models/User";
import '../scss/signin.css';
import { Link, RouteComponentProps, withRouter } from "react-router-dom";
import { connect, Provider } from "react-redux";
import { RootState } from "../store";
import { registerUser } from "../services/users.service";
import { store } from "../App";
import { fetchUsers } from "../store/users-actions";



interface State{
    username: string;
    password: string;
    registered: boolean;
}


interface Props extends RouteComponentProps<any>{
    users: User[];
}




class Signup extends Component<Props, State>{
    constructor(props:Props){
        super(props)
        this.state ={ username: "", password:  "", registered: false}
    }
    render(){
        return(
            <>
            <NavBar />
           
            <div className="signup text-center">
            <div className="form-signin">
            <h1 className="h3 mb-3 font-weight-normal">Kreirajte novi nalog</h1>
            <label className="sr-only">User Name</label>
             <input name="username"  className="form-control" placeholder="Korisničko ime" onChange = {(event) => {
                 this.setState({username: event.target.value});
                 this.setState({registered: false})
                 setTimeout(() => {
                   this.props.users.forEach(element => {
                       if(this.state.username.toLocaleLowerCase() === element.id){
                           this.setState({registered: true})
                       }
                   });
             }, 100);   
            }} />
            <label className="sr-only">Password</label>
            <input name="password" type="password" className="form-control" placeholder="Lozinka" onChange = {(event) => {
                this.setState({password: event.target.value})
            }} />
            <div className="checkbox mb-3">
             </div>  
             <Provider store = {store}>
            <button className="btn btn-lg btn-primary" onClick = {() => {
                if(this.state.registered === false){
                    const newUser: User = 
                        {
                        id: this.state.username.toLocaleLowerCase(),
                        username: this.state.username,
                        password: this.state.password,
                        speed: 0};  
                     registerUser(newUser);
                     alert("Uspešno ste kreirali nalog.");
                     setTimeout(() => {store.dispatch(fetchUsers());
                    }, 100);
                     this.props.history.push('/login');
                }
                else{
                    alert("Uneto korisničko ime je zauzeto. Molimo vas unesite drugo korisničko ime.")
                }
            }}>Kreiraj Nalog</button>
            </Provider>
            </div>
            </div>
            <Link to="/" className="navbar-brand btn btn-danger margintop">Nazad</Link>
            <p className="mt-5 mb-3 text-muted">&copy;Nnoitora 2019</p>
            </>
        )
    }
}

function mapStateToProps(state: RootState) {
    return {
      users: state.users
    };
}


export default connect(mapStateToProps)(withRouter(Signup));