import React, { Component } from "react";
import NavBar from "./Navbar";
import '../scss/about.css';
import Particles from "react-particles-js";
import { particlesOpt } from './particles';


class About extends Component{
    render(){
        return(
            <>
            <NavBar />
            <div>
            <h1 className = "aboutHead" >Brzo Čitanje</h1>
            <aside className = "asideleft">Milioni ljudi širom sveta izjavili su da je lako i tečno ovladavanje tehnikama
                                           brzog čitanja jedan od najvrednijih i najvažnijih događaja u njihovom životu.</aside>
            <div className = "aboutImage">
            <Particles className = "particles" params= {particlesOpt} />
            </div>
            <aside className = "asideright"><h4>Ova aplikacija ima šest osnovnih ciljeva:</h4>
                    <ul>1. Da znatno poboljša vašu brzinu čitanja</ul>
                    <ul>2. Da održi i poboljša razumevanje onoga što čitate.</ul>
                    <ul>3. Da uveća i olakša efikasnije korišćenje funkcije oka i mozga.</ul>
                    <ul>4. Da vam pomogne da obogatite svoj rečnik i povećate opšti nivo znanja.</ul>
                    <ul>5. Da vam uštedi vreme.</ul>
                    <ul>6. Da vam ulije samopouzdanje.</ul>
            </aside>
            <p className="mt-5 mb-3 copyright">&copy;Nnoitora 2019</p>
            </div>
            </>
        )
    }
}

export default About;

