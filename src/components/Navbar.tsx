import React, { Component } from "react";
import { NavLink, Link } from "react-router-dom";
import { User } from "../models/User";
import { RootState } from "../store";
import { connect, Provider } from "react-redux";
import { store } from "../App";
import { logOut } from "../store/users-actions";
import { initialState } from "../store/users-reducer";

interface Props{
  users: User[]
}

interface State{
  username: string;
}

class NavBar extends Component<Props, State>{
  constructor(props:Props){
    super(props)
    this.state ={ username: this.props.users[0].username }
}
render(){
     if(this.props.users[0] !== undefined){
       return(
        <nav className="navbar navbar-expand-md navbar-light fixed-top bg-light" style={{position:"sticky"}}>
          
        <Link to="/" className="navbar-brand">Brzo Čitanje</Link>

        <button className="navbar-toggler" type="button"
                 data-toggle="collapse" data-target="#navbarCollapse" 
                 aria-controls="navbarCollapse"
                 aria-expanded="false" aria-label="Toggle navigation">

            <span className="navbar-toggler-icon"></span>

        </button>
        
        <div className="collapse navbar-collapse" id="navbarCollapse">
          
          <ul className="navbar-nav mr-auto">
    
          <li className="nav-item">
          <NavLink to="/" className="nav-link" exact activeStyle = {
            {color : 'red'}
          }>Početna</NavLink>
          </li>

          <li className="nav-item">
          <NavLink to="/user"  className="nav-link" exact activeStyle = {
            {color : 'red'}
          }>{this.props.users[0].username}</NavLink>
          </li>
          
          
          <li className="nav-item">
          <NavLink to="/about" className="nav-link" exact activeStyle = {
            {color : 'red'}
          }>O nama</NavLink>
          </li>

            
          </ul>
          <Provider store = {store}>
            <button className="btn btn-outline-danger my-2 my-sm-0" 

            onMouseOver = {() =>{
              if(this.props.users[0].username !== "Korisnik"){
                this.setState({username: "Odjavi se"})
              }
            }} 

            onMouseOut = {() => {
              this.setState({username: this.props.users[0].username})
            }}
            
            onClick = {() => {
              if(this.props.users[0].username !== "Korisnik"){
                if(window.confirm("Da li ste sigurni da želite da se odjavite?")){
                store.dispatch(logOut(initialState));
                }
              }
            }}>{this.state.username}</button>
          </Provider>
        </div>
        
        
        </nav>
       )}
       else{
        return(
         
          <nav className="navbar navbar-expand-md navbar-light fixed-top bg-light" style={{position:"sticky"}}>
            
          <Link to="/" className="navbar-brand">Brzo Čitanje</Link>
  
          <button className="navbar-toggler" type="button"
                   data-toggle="collapse" data-target="#navbarCollapse" 
                   aria-controls="navbarCollapse"
                   aria-expanded="false" aria-label="Toggle navigation">
  
              <span className="navbar-toggler-icon"></span>
  
          </button>
          
          <div className="collapse navbar-collapse" id="navbarCollapse">
            
            <ul className="navbar-nav mr-auto">
  
            <li className="nav-item">
            <NavLink to="/" className="nav-link" exact activeStyle = {
              {color : 'red'}
            }>Početna</NavLink>
            </li>
  
            <li className="nav-item">
            <NavLink to="/user" className="nav-link" exact activeStyle = {
              {color : 'red'}
            }>Korisnik</NavLink>
            </li>
            
            
            <li className="nav-item">
            <NavLink to="/about" className="nav-link" exact activeStyle = {
              {color : 'red'}
            }>O nama</NavLink>
            </li>
  
              
            </ul>
              
              <Link to="/user"><label className="btn btn-outline-danger my-2 my-sm-0">{this.state.username}</label></Link>
  
          </div>
          
          
          </nav>
         )
       }
    }
}


function mapStateToProps(state: RootState){
  return{
      users: state.users
  };
}

export default connect(mapStateToProps)(NavBar);