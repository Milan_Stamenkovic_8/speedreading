import React, { Component } from "react";
import NavBar from "./Navbar";
import '../scss/signin.css';
import { Link, RouteComponentProps, withRouter } from "react-router-dom";
import { Provider, connect } from "react-redux";
import { store } from "../App";
import { checkUser, fetchUsers } from "../store/users-actions";
import { User } from "../models/User";
import { RootState } from "../store";



interface Props extends RouteComponentProps<any>{
    users: User[];
}

interface State{
    username: string;
    password: string;
}

class Login extends Component<Props, State>{
    constructor(props:Props){
        super(props)
        this.state ={ username: "", password:  ""}
    }
    render(){
        return(
            <>
            <NavBar />
            <div className="signup text-center">
            <Provider store = {store}>
             <div className="form-signin">
            <img className="mb-4" src="/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72" />
            <h1 className="h3 mb-3 font-weight-normal">Prijavite se na postojeći nalog</h1>
            <label className="sr-only">Username</label>
            <input type="username" id="inputUsername" className="form-control" placeholder="Korisničko ime"  onChange = {(event) => {
                this.setState({username: event.target.value.toLocaleLowerCase()})
            }} />      
            <label className="sr-only">Password</label>
            <input type="password" id="inputPassword" className="form-control" placeholder="Lozinka" onChange = {(event) => {
                this.setState({password: event.target.value})
            }} />
            <Link to="/signup" className="btn-primary" >Ukoliko nemate nalog, kliknite ovde kako bi ste kreirali svoj nalog</Link>
            <div className="checkbox mb-3">
             <label>
            </label>
            </div>
            <button className="btn btn-lg btn-primary" onClick = {() =>{
                if(this.state.username === "" || this.state.username === undefined){
                    alert("Morate uneti korisničko ime.")
                }
                else if(this.props.users.length > 1){
                store.dispatch(checkUser(this.state.username));
                }
                
                setTimeout(() => {
                if(this.props.users[0] === undefined){
                    alert("Uneli ste pogrešno korisničko ime.");
                    store.dispatch(fetchUsers());
                }
                else if(this.state.password !== this.props.users[0].password){
                    alert("Uneli ste pogrešnu lozinku.");
                }
                else{
                    alert("Prijavljivanje uspešno.");
                    this.props.history.push('/');
                }}, 100);
            }}>Prijavi se</button>

            </div>
            </Provider>
            </div>
            <Link to="/" className="navbar-brand btn btn-danger margintop">Nazad</Link>
            <p className="mt-5 mb-3 text-muted">&copy;Nnoitora 2019</p>
            </>
        )
    }
}

function mapStateToProps(state: RootState){
    return{
        users: state.users
    };
}

export default connect(mapStateToProps)(withRouter(Login)); 