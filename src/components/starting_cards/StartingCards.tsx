import React, { Component } from "react";
import "../../scss/starting_card.css";
import Card from "./Card";



class StartingCards extends Component{
    render(){
        return(
            <div className="text-center">
            <Card naslov = {card_one_title} 
                  opis = {card_one_description}
                  classImage = "slika1"  
                  path= "/practice" />
            <Card naslov = {card_two_title} 
                  opis = {card_two_description}
                  classImage = "slika2"
                  path= "/test" />
            <Card naslov = {card_three_title} 
                  opis = {card_three_description} 
                  classImage = "slika3"  
                  path= "/understanding"/>
          </div>

        )
    }

}


export default StartingCards;


const card_one_title = "Probajte različite brzine čitanja";
const card_two_title = "Izmerite brzinu čitanja";
const card_three_title = "Test razumevanja";


const card_one_description = "Povećajte brzinu čitanja do 5 puta uz korišćenje jednostavnih tehnika. Probajte različite brzine čitanja i vežbom povećajte brzinu čitanja i poboljšajte periferni vid.";
const card_two_description = "Testiraje svoje tehnike čitanja i proverite brzinu kojom čitate. Ovde možete izmeriti brzinu čitanja u rečima u minutu. Saznajte svoju brzinu čitanja."
const card_three_description = "Ukoliko ste izmerili brzinu čitanja jednog od tekstova, proverite nivo zapamćenog nakon čitanja trenutnom brzinom."

