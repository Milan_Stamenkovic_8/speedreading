import React, { Component } from "react";
import "../../scss/starting_card.css";
import { Link } from "react-router-dom";


interface State{
}

interface Props{
   naslov: string
   opis: string
   classImage: string;
   path: string;
}


class Card extends Component<Props, State>{
    render(){
        if(!this.props){
            return ""
        }
        return(
            <Link to = {this.props.path}>
            <div className={`box ${this.props.classImage}`}>
            <h2 className="text-center naslov">{this.props.naslov}</h2>
            <div className="karticaDiv">{this.props.opis}</div>
            </div>
            </Link>
        );
    }

}


export default Card;
