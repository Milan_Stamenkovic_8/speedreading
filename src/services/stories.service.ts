const urlstories = 'http://localhost:3002/stories/'

export function getStories(){
    return fetch(urlstories)
    .then(response=>response.json())
}
