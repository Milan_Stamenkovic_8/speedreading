const urlusers = 'http://localhost:3002/users/'


export function getAllUsers(){
    return fetch(urlusers)
    .then(response=>response.json())
}



export function registerUser(data = {}){
    return fetch(urlusers, {
        method: "POST",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        headers: {
            "Content-Type": "application/json",
        },
        redirect: "follow",
        referrer: "no-referrer",
        body: JSON.stringify(data),
    })
    .then(response=>response.json());
}

export function deleteUser(id: string){
    return fetch(`${urlusers}${id}`, {
        method: "DELETE",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        headers: {
            "Content-Type": "application/json",
        },
        redirect: "follow",
        referrer: "no-referrer",
    })
}

export function updateUser(id: string, data = {}){
    return fetch(`${urlusers}${id}`, {
        method: "PUT",
        mode: "cors", 
        cache: "no-cache", 
        credentials: "same-origin", 
        headers: {
            "Content-Type": "application/json",
        },
        redirect: "follow", 
        referrer: "no-referrer",
        body: JSON.stringify(data), 
    })
    .then(response => response.json()); 
}